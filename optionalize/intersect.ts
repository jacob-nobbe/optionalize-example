/** merges types A and B (I think?)*/
type Intersection<A, B> = A & B extends infer U
  ? { [P in keyof U]: U[P] }
  : never

export type { Intersection }
