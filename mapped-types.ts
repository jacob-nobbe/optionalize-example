import * as FirestoreSchema from './firestore-schema'
import { Optionalize } from './optionalize/optionalize'
import { Intersection } from './optionalize/intersect'

type Category = Optionalize<FirestoreSchema.Category>

type ExtendedTransactionInfo =
  Optionalize<FirestoreSchema.ExtendedTransactionInfo>

// this is a bit annoying, maybe a helper is possible?
type TaggedTransaction = Optionalize<
  Intersection<
    Omit<FirestoreSchema.TaggedTransaction, 'extendedInfo'>,
    { extendedInfo?: ExtendedTransactionInfo }
  >
>

export type { Category, TaggedTransaction, ExtendedTransactionInfo }
