import type { Option } from '@wentools/option'
import type { Intersection } from './intersect'
import type { Matching, NonMatching } from './match'

/** Pick properties from T that are Option type */
type Options<T> = Pick<T, Matching<T, Option<any>>>

/** Pick properties from T that are not Option type */
type NotOptions<T> = Pick<T, NonMatching<T, Option<any>>>

/** Unwrap Option properties and make optional*/
type UnwrapOptionalized<OptionalizedType extends Object> = Intersection<
  NotOptions<OptionalizedType>,
  {
    [Property in keyof Options<OptionalizedType>]?: OptionalizedType[Property] extends Option<
      infer U
    >
      ? U
      : never
  }
>

export type { UnwrapOptionalized }
